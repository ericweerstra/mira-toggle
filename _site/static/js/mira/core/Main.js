Namespace.register('mira.core').Main = (function() {

    var Main = function() {

        this._triggerController = new mira.toggle.TriggerController.getInstance();

        document.addEventListener('DOMContentLoaded', this._initialize());

    };

    var p = Main.prototype;

    p._initialize = function() {

        this._createToggles();
        this._createTriggers();

    };

    p._createToggles = function() {

        var toggleElements = document.querySelectorAll('[data-toggle]'),
            i, length;

        for (i = 0, length = toggleElements.length; i < length; i++) {
            new mira.toggle.Toggle(toggleElements[i]);
        }

    };

    p._createTriggers = function() {

        var triggerElements = document.querySelectorAll('[data-toggle-trigger]'),
            i, length;

        for (i = 0, length = triggerElements.length; i < length; i++) {
            this._triggerController.createTrigger(triggerElements[i]);
        }

    };

    var _instance;

    return {

        getInstance: function() {

            if (!_instance) {
                _instance = new Main();
            }

            return _instance;
        }
    };

})();