Namespace.register('mira.toggle').TriggerController = (function() {

    var TriggerController = function() {};

    var p = TriggerController.prototype;

    p.createTrigger = function(element) {

        var type = element.getAttribute('data-toggle-trigger');

        switch(type) {

            case 'element':
                new mira.toggle.ElementTrigger(element);
                break;

            case 'hash':
                new mira.toggle.HashTrigger(element);
                break;

            case 'body':
                new mira.toggle.BodyTrigger(element);
                break;

        }

    };

    var _instance;

    return {

        getInstance: function() {

            if (!_instance) {
                _instance = new TriggerController();
            }

            return _instance;
        }
    };

}());