/*
* Todo:
* Check if toggle and toggle trigger exists
* It should be possible to handle more then one trigger for the same toggle
*/

 Namespace.register('mira.toggle').Deactivator = (function() {

    var Deactivator = function(trigger, toggleId, toggleTriggerId, event) {

        this._trigger = trigger;

        var manager = mira.toggle.Manager.getInstance();

        this._toggle = manager.getToggleById(toggleId);
        this._toggleTrigger = manager.getToggleById(toggleTriggerId);
        this._deactivateFunc = this._deactivate.bind(this);
        this._eventName = !event ? 'click' : event;

        this._initialize();

    };

    var p = Deactivator.prototype;

    p._initialize = function() {

        // listen to the toggle when it gets active and bind de deactivation event
        Observer.subscribe(this._toggle, 'active', this._handleToggleActive.bind(this));

        // prevent that the toggle or the toggle-trigger element is triggering the deactivation
        this._toggle.getElement().addEventListener(this._eventName, function(e) { e.stopPropagation() });
        this._toggleTrigger.getElement().addEventListener(this._eventName, function(e) { e.stopPropagation() });

    };

    p._handleToggleActive = function() {
        this._trigger.addEventListener(this._eventName, this._deactivateFunc);
    };

    p._deactivate = function() {

        this._toggle.deactivate();
        this._toggleTrigger.deactivate();
        this._trigger.removeEventListener(this._eventName, this._deactivateFunc);

    };

    return Deactivator;

}());