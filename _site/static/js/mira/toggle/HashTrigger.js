Namespace.register('mira.toggle').HashTrigger = (function() {

    var _super = mira.toggle.Trigger;

    var HashTrigger = function(element) {

        _super.call(this, element);

        this._href = this._element.getAttribute('href');
        this._hash = null;
        this._currentHash = null;

    };

    var p = HashTrigger.prototype = Object.create(_super.prototype);

    //p._initialize = function() {};

    p._setup = function() {
        this._trigger();
        this._bindEvents()
    };

    p._getTargets = function() {

        this._hash = window.location.hash;

        if (this._hash === '' && this._currentHash) {

            this._targets = this._getTargetElements(this._currentHash);
            this._hash = this._currentHash;
            this._currentHash = null;

        }
        else if (this._hash !== '' && this._currentHash && this._checkCurrentTarget(this._currentHash)) {
            this._currentHash = this._hash;
        }
        else if (this._hash !== '') {

            this._targets = this._getTargetElements(this._hash);
            this._currentHash = this._hash;

        }

    };

    p._bindEvents = function() {
        window.addEventListener('hashchange', this._trigger.bind(this));
    };

    p._trigger = function() {

        this._getTargets();

        if (this._hash === this._href) {
            this._toggle();
        }

    };

    //p._toggle = function() {};

    p._getTargetElements = function(targets) {
        return targets.substring(1).split('&');
    };

    p._checkCurrentTarget = function(hash) {

        var toggle = this._manager.getToggleById(hash.substring(1));

        if (toggle && !toggle.getGroup()) {

            toggle.deactivate();

            return true;

        }

        return false;

    };

    return HashTrigger;

}());