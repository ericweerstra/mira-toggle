Namespace.register('mira.toggle').Group = (function() {

    var Group = function() {
        this._toggles = [];
        this._activeToggles = [];
    };

    var p = Group.prototype;

    p.register = function(toggle) {

        this._toggles.push(toggle);
        Observer.subscribe(toggle, 'toggle', this._toggle.bind(this));

        this._getActiveToggles();

    };

    p._getActiveToggles = function() {

        var i, length,
            toggle;

        for (i = 0, length = this._toggles.length; i < length; i++) {

            toggle = this._toggles[i];

            if (toggle.getState() === 'active') {
                this._activeToggles.push(toggle)
            }
        }

    };

    p._toggle = function(target) {

        var i, length;

        if (this._activeToggles.length > 0) {

            for (i = 0, length = this._activeToggles.length; i < length; i++) {
                this._activeToggles[i].deactivate();
            }

            this._activeToggles = [];

        }

        target.activate();
        this._activeToggles.push(target);


    };

    return Group;

}());