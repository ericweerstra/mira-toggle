Namespace.register('mira.toggle').BodyTrigger = (function() {

    var _super = mira.toggle.Trigger;

    var BodyTrigger = function(element) {

        //this._toggleElement = null;

        _super.call(this, element);

    };

    var p = BodyTrigger.prototype = Object.create(_super.prototype);

    //p._initialize = function() {};

    p._setup = function() {

        this._getTargets();

        /*Observer.subscribe(this._manager.getToggleById(this._targets[0]), 'active', function() {
            console.log('yo');
        })*/

        /*var toggleAttribute = this._element.getAttribute('data-toggle');

        if (toggleAttribute) {
            this._toggleElement = new mira.toggle.Toggle(this._element);
        }

        this._getTargets();
        this._bindEvents()*/

    };

    p._getTargets = function() {

        var targetAttribute = this._element.getAttribute('data-toggle-target');

        if (targetAttribute) {
            this._targets = targetAttribute.split(',');
        }

    };

    p._bindEvents = function() {

        var eventAttr = this._element.getAttribute('data-toggle-event'),
            eventName = !eventAttr ? 'click' : eventAttr;

        this._element.addEventListener(eventName, this._trigger.bind(this));

    };

    p._trigger = function() {

        if (this._toggleElement) {

            this._toggleElement.toggle();
            Observer.publish(this, 'active');

        }

        this._toggle();

    };

    //p._toggle = function() {};

    return BodyTrigger;

}());

/*
Namespace.register('mira.toggle').BodyTrigger = (function() {

    var _super = mira.toggle.Trigger;

    var BodyTrigger = function(element) {

        _super.call(this, element);

        var eventAttr = this._element.getAttribute('data-toggle-event');
        this._eventName = !eventAttr ? 'click' : eventAttr;

    };

    var p = BodyTrigger.prototype = Object.create(_super.prototype);

    //p._initialize = function() {};

    p._setup = function() {

        this._getTargets();

        */
/*var target,
            i, length;

        for (i = 0, length = this._targets.length; i < length; i++) {

            target = this._manager.getToggleById(this._targets[i]);
            //Observer.subscribe(target, 'active', this._trigger.bind(this));

        }*//*


    };

    p._getTargets = function() {

        var targetAttribute = this._element.getAttribute('data-toggle-target');

        if (targetAttribute) {
            this._targets = targetAttribute.split(',');
        }

    };

    p._bindEvents = function() {

        */
/*var eventAttr = this._element.getAttribute('data-toggle-event'),
            eventName = !eventAttr ? 'click' : eventAttr;*//*


        this._element.addEventListener(this._eventName, this._trigger.bind(this), false);

    };

    p._trigger = function(target) {
        //console.log(target)
    };

    //p._toggle = function() {};

    return BodyTrigger;

}());*/
