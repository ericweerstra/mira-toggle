
Namespace.register('mira.toggle').Toggle = (function() {

    var Toggle = function(element) {

        this._element = element;
        this._attribute = 'data-toggle';
        this._id = null;

        // set id
        this._setId();

        // register
        mira.toggle.Manager.getInstance().register(this);

    };

    var p = Toggle.prototype;

    p.toggle = function() {

        // if part of group let the group handle it
        if (this.getGroup()) {
            Observer.publish(this, 'toggle', this);
        }
        else {
            this.getState() === 'inactive' ? this.activate() : this.deactivate();
        }

    };

    p._setId = function() {
        this._id = this._element.getAttribute('id');
    };

    p.activate = function() {

        this._element.setAttribute(this._attribute, 'active');
        //this._reflow(this._element);
        Observer.publish(this, 'active', this);

    };

    p.deactivate = function() {

        this._element.setAttribute(this._attribute, 'inactive');
        //this._reflow(this._element);

        Observer.publish(this, 'inactive', this);

    };

    p.getId = function() {
        return this._id;
    };

    p.getElement = function() {
        return this._element;
    };

    p.getState = function() {
        return this._element.getAttribute(this._attribute)
    };

    p.getGroup = function() {
        return this._element.getAttribute('data-group')
    };

    /*p._reflow = function(element) {
        element.className = element.className;
    };*/

    return Toggle;

}());