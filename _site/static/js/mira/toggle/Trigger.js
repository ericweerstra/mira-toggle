Namespace.register('mira.toggle').Trigger = (function() {

    var Trigger = function(element) {

        this._element = element;
        this._targets = [];
        this._manager = mira.toggle.Manager.getInstance();

        this._initialize();

    };

    var p = Trigger.prototype;

    p._initialize = function() {

        this._setup();
        //this._bindEvents();

    };

    p._setup = function() {};

    //p._getTargets = function() {};

    p._bindEvents = function() {};

    //p._trigger = function() {};

    p._toggle = function() {

        var target,
            i, length;

        for (i = 0, length = this._targets.length; i < length; i++) {

            target = this._manager.getToggleById(this._targets[i]);

            if (target) {
                target.toggle();
            }

        }

    };

    return Trigger;

}());