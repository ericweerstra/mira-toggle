Namespace.register('mira.toggle').Manager = (function() {

    var Manager = function() {

        this._togglers = [];
        this._groups = [];

    };

    var p = Manager.prototype;

    p.register = function(toggle) {

        this._togglers.push(toggle);

        // look for groups
        this._manageGroup(toggle);

    };

    p.getToggleById = function(id) {

        var i, length,
            toggle;

        for (i = 0, length = this._togglers.length; i < length; i++) {

            toggle = this._togglers[i];

            if (toggle.getId() === id) {
                return toggle;
            }

        }

    };

    p._manageGroup = function(toggle) {

        // check if part of group
        var group = toggle.getGroup();

        if (group) {

            // check if group exists, if not create
            if (!this._groups[group]) {
                this._groups[group] = new mira.toggle.Group(group);
            }

            // register in group
            this._groups[group].register(toggle);

        }

    };


    var _instance;

    return {

        getInstance: function () {

            if (!_instance) {
                _instance = new Manager();
            }

            return _instance;
        }
    };

})();