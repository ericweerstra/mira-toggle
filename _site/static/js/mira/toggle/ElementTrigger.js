Namespace.register('mira.toggle').ElementTrigger = (function() {

    var _super = mira.toggle.Trigger;

    var ElementTrigger = function(element) {

        this._toggleElement = null;

        _super.call(this, element);

    };

    var p = ElementTrigger.prototype = Object.create(_super.prototype);

    //p._initialize = function() {};

    p._setup = function() {

        var toggleAttribute = this._element.getAttribute('data-toggle');

        if (toggleAttribute) {
            this._toggleElement = new mira.toggle.Toggle(this._element);
        }

        this._getTargets();
        this._bindEvents()

    };

    p._getTargets = function() {

        var targetAttribute = this._element.getAttribute('data-toggle-target');

        if (targetAttribute) {
            this._targets = targetAttribute.split(',');
        }

    };

    p._bindEvents = function() {

        var eventAttr = this._element.getAttribute('data-toggle-event'),
            eventName = !eventAttr ? 'click' : eventAttr;

        this._element.addEventListener(eventName, this._trigger.bind(this));

    };

    p._trigger = function() {

        if (this._toggleElement) {
            this._toggleElement.toggle();
        }

        this._toggle();

    };

    //p._toggle = function() {};

    return ElementTrigger;

}());