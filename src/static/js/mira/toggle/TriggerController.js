/**
 * TriggerController
 * Factory that creates trigger Class instances according to type.
 *
 * @module mira/toggle/triggercontroller
 */

// Register namespace
Namespace.register('mira.toggle').TriggerController = (function() {

    'use strict';

    /**
     * @class TriggerController
     * @constructor
    */
    var TriggerController = function() {};

    var p = TriggerController.prototype;

    /**
     * Gets the type from element and creates diffrent trigger instances according to it.
     * @method createTrigger
     * @public
     * @param {object} element. The HTML node were the type is set from the data-toggle-trigger attributes value.
     */
    p.createTrigger = function(element) {

        var type = element.getAttribute('data-toggle-trigger');

        switch(type) {

            case 'element':
                new mira.toggle.ElementTrigger(element);
                break;

            case 'hash':
                new mira.toggle.HashTrigger(element);
                break;

            case 'body':
                new mira.toggle.BodyTrigger(element);
                break;

        }

    };


    /**
   	 * Stores the instance of this class if there is one.
   	 * @property _instance
   	 * @type {Object}
   	 * @default null
   	 **/
    var _instance;

    return {

        /**
       	 * Returns the instance object if there is one otherwise one will be created so there can only be one. This turns this Class into a Singleton.
         * @method getInstance
       	 * @private
       	 **/

        getInstance: function() {

            if (!_instance) {
                _instance = new TriggerController();
            }

            return _instance;
        }
    };

}());