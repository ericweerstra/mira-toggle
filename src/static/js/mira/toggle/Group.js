/**
 * Group
 * Register Toggles and listens if one gets toggled. If a group member is allready active it gets deactivated first before the new one gets activated.
 *
 * @module mira/toggle/group
 */

// Register namespace
Namespace.register('mira.toggle').Group = (function() {

    'use strict';

    /**
     * @class Group
     * @constructor
     */
    var Group = function() {

        this._toggles = [];
        this._activeToggles = [];

    };

    var p = Group.prototype;

    /**
     * Collects Toggle instances and subscribes the to the Observer.
     * @method register
     * @public
     * @param {Toggle} toggle. Class instance of the Toggle that you want to register to the group.
     */
    p.register = function(toggle) {

        this._toggles.push(toggle);

        // listen if the toggle gets toggled
        Observer.subscribe(toggle, 'toggle', this._toggle.bind(this));

        this._getActiveToggles();

    };

    /**
     * Collects the active toggles by calling the getState method on the Toggle.
     * @method _getActiveToggles
     * @private
     */
    p._getActiveToggles = function() {

        var self = this;

        this._toggles.forEach(function(toggle) {

            if (toggle.getState() === 'active') {
                self._activeToggles.push(toggle)
            }

        });

    };

    /**
     * Checks the active Toggles and if one is active it gets deactivated before the new one gets activated.
     * @method _toggle
     * @private
     */
    p._toggle = function(target) {

        if (this._activeToggles.length > 0) {

            this._activeToggles.forEach(function(toggle) {
                toggle.deactivate();
            });

            this._activeToggles = []

        }

        target.activate();
        this._activeToggles.push(target);

    };

    return Group;

}());