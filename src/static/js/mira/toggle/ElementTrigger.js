/**
 * ElementTrigger
 * This is used for HTML elements to trigger toggles according to the target attribute. This element itself can also be a Toggle.
 *
 * @module mira/toggle/elementtrigger
 */

// Register namespace
Namespace.register('mira.toggle').ElementTrigger = (function() {

    'use strict';

    /**
   	 * Reference to the Trigger base this class inherits from
   	 * @property _super
   	 * @type {Trigger} _super
   	 **/
    var _super = mira.toggle.Trigger;

    /**
     * @class ElementTrigger
     * @extends Trigger
     * @constructor
     * @param {object} element. The HTML node that should behave like a trigger.
     */
    var ElementTrigger = function(element) {

        this._toggleElement = null;
        this._eventName = null;
        this._triggerFunc = this._trigger.bind(this);

        // call the base constuctor and pass element
        _super.call(this, element);

    };

    var p = ElementTrigger.prototype = Object.create(_super.prototype);

    /**
     * This method is called by the Trigger Base Class and checks if this Trigger itself is also a toggle.
     * @method _setup
     * @protected
     */
    p._setup = function() {

        // check if the element itself is a toggle
        if (this._element.getAttribute('data-toggle')) {
            this._toggleElement = this._manager.getById(this._element.id, 'toggle');
        }

        this._getEventName();

    };

    /**
     * This method is called by the Trigger Base Class and it gets the target from the HTML node data-toggle-target attribute value.
     * @method _getTargets
     * @protected
     */
    p._getTargets = function() {

        var targetAttribute = this._element.getAttribute('data-toggle-target');

        if (targetAttribute) {
            this._targets = targetAttribute.split(',');
        }

    };

    /**
     * This method is initially called by the Trigger Base Class and it binds the trigger event to the HTML node.
     * @method bindEvents
     * @public
     */
    p.bindEvents = function() {
        this._element.addEventListener(this._eventName, this._triggerFunc);
    };

    /**
     * This method removes the event that triggers the HTML node.
     * @method removeEvents
     * @public
     */
    p.removeEvents = function() {
        this._element.removeEventListener(this._eventName, this._triggerFunc);
    };

    /**
     * Get the event name from the HTML node data-toggle-event attribute value if it has one. Otherwise use click as default.
     * @method _getEventName
     * @private
     */
    p._getEventName = function() {

        // if there is an event name attribute use this otherwise use click
        var eventAttr = this._element.getAttribute('data-toggle-event');
        this._eventName = !eventAttr ? 'click' : eventAttr;

    };

    /**
     * If this element is also a Toggle then Toggle it. Also call the _toggle method within the Base Class.
     * @method _trigger
     * @private
     */
    p._trigger = function(e) {

        e.preventDefault();

        // if the element itself is a toggle toggle it
        if (this._toggleElement) {
            this._toggleElement.toggle();
        }

        // and call toggle on the target
        this._toggle();

    };

    return ElementTrigger;

}());