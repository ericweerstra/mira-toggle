/**
 * BodyTrigger
 * This class extends the Element Trigger. It triggers only when his targets are active It can be usefull incase the mouse leaves a dropdown to trigger this to its original state.
 *
 * @module mira/toggle/bodytrigger
 */

// Register namespace
Namespace.register('mira.toggle').BodyTrigger = (function() {

    'use strict';

    /**
   	 * Reference to the Element Trigger Class this Class inherrits from
   	 * @property _super
   	 * @type {ElementTrigger} _super
   	 **/
    var _super = mira.toggle.ElementTrigger;

    /**
     * @class BodyTrigger
     * @extends ElementTrigger
     * @constructor
     * @param {object} element. The HTML node that should behave like a trigger.
     */
    var BodyTrigger = function(element) {

        this._toggles = [];
        this._deactivateFunc = this._deactivate.bind(this);
        this._elementTrigger = null;

        // call the base constructor
        _super.call(this, element);

    };

    var p = BodyTrigger.prototype = Object.create(_super.prototype);

    /**
     * This method is called by the Trigger Base Class. It gets the Toggle or Trigger through the manager and subscribes them to the Observer to check if they get active.
     * @method bindEvents
     * @public
     */
    p.bindEvents = function() {

        var self = this,
            target;

        this._targets.forEach(function(element) {

            target = self._manager.getById(element, 'toggle');

            // prevent that the toggle or the trigger element is triggering the deactivation
            target.getElement().addEventListener(self._eventName, function(e) { e.stopPropagation() });

            // listen to the toggle or trigger to get active before binding the deactivation event
            Observer.subscribe(target, 'active', self._handleToggleActive.bind(self));

        });

    };

    /**
     * If one of the Toggles gets active bind the event to the element and remember the Toggles if it is a trigger remove the trigger event.
     * @method _handleToggleActive
     * @private
     * @param {Toggle} toggle
     */
    p._handleToggleActive = function(toggle) {

        // store the toggles that are active
        this._toggles.push(toggle);

        // check if the toggle is a trigger and remove the event so it won't trigger anymore
        var trigger = this._manager.getById(toggle.getId(), 'trigger');

        if (trigger) {

            trigger.removeEvents();
            this._elementTrigger = trigger; // remember the trigger so we can restore events later on

        }

        // add listener to the body
        this._element.addEventListener(this._eventName, this._deactivateFunc);

    };

    /**
     * Deactivates the Toggles and remove the listener from the element.
     * @method _deactivate
     * @private
     */
    p._deactivate = function() {

        // deactivate the toggles
        this._toggles.forEach(function(element) {
            element.deactivate();
        });

        // bind the trigger event to the element again
        if (this._elementTrigger) {

            this._elementTrigger.bindEvents();
            this._elementTrigger = null;

        }

        // remove listener to the body
        this._element.removeEventListener(this._eventName, this._deactivateFunc);

        // empty the toggles array
        this._toggles = [];

    };

    return BodyTrigger;

}());