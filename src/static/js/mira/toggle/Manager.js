/**
 * Manager
 * Singleton for managing Toggle or Trigger instances
 *
 * @module mira/toggle/manager
 */

// Register namespace
Namespace.register('mira.toggle').Manager = (function() {

    'use strict';

    /**
     * @class Manager
     * @constructor
     */
    var Manager = function() {

        this._togglers = [];
        this._groups = [];
        this._triggers = [];

    };

    var p = Manager.prototype;

    /**
     * Collects Toggle or Trigger instances and in case of a Toggle calls _manageGroup method.
     * @method register
     * @public
     * @param {Object} element. Class instance of the Toggle or Trigger that you want to register.
     * @param {String} type. Determines if its a Toggle or Trigger
     */
    p.register = function(element, type) {

        switch(type) {

            case 'toggle':

                this._togglers.push(element);
                this._manageGroup(element);

                break;

            case 'trigger':
                this._triggers.push(element);
                break;

        }

    };

    /**
     * Returns the instance according to ID.
     * @method getById
     * @public
     * @param {String} id.
     * @param {String} type. Determines if its a Toggle or Trigger
     */
    p.getById = function(id, type) {

        var i, length,
            elements = type === 'toggle' ? this._togglers : this._triggers,
            element;

        for (i = 0, length = elements.length; i < length; i++) {

            element = elements[i];

            if (element.getId() === id) {
                return element;
            }

        }

    };

    /**
     * Creates a Group if it not yet excists and register the Toggle with this group if its a member.
     * @method _manageGroup
     * @private
     * @param {Toggle} toggle.
     */
    p._manageGroup = function(toggle) {

        // check if part of group
        var group = toggle.getGroup();

        if (group) {

            // check if group exists, if not create
            if (!this._groups[group]) {
                this._groups[group] = new mira.toggle.Group(group);
            }

            // register in group
            this._groups[group].register(toggle);

        }

    };


    /**
   	 * Stores the instance of this class if there is one.
   	 * @property _instance
   	 * @type {Object}
   	 * @default null
   	 **/

    var _instance;

    return {

        /**
       	 * Returns the instance object if there is one otherwise one will be created so there can only be one. This turns this Class into a Singleton.
         * @method getInstance
       	 * @private
       	 **/

        getInstance: function () {

            if (!_instance) {
                _instance = new Manager();
            }

            return _instance;
        }
    };

})();