/**
 * HashTrigger
 * This is used to trigger Toggles by hashchange.
 *
 * @module mira/toggle/hashtrigger
 */

// Register namespace
Namespace.register('mira.toggle').HashTrigger = (function() {

    'use strict';

    /**
   	 * Reference to the Trigger base this class inherrits from
   	 * @property _super
   	 * @type {Trigger} _super
   	 **/
    var _super = mira.toggle.Trigger;

    /**
     * @class ElementTrigger
     * @extends Trigger
     * @constructor
     * @param {object} element. The HTML node wich can trigger the hash.
     */
    var HashTrigger = function(element) {

        this._href = element.getAttribute('href');
        this._hash = null;
        this._currentHash = null;

        // call base constructor
        _super.call(this, element);

    };

    var p = HashTrigger.prototype = Object.create(_super.prototype);

    /**
     * This method is called by the Trigger Base Class it calls the _trigger method once to set the initial state
     * @method _setup
     * @protected
     */
    p._setup = function() {
        this._trigger();
    };

    /**
     * This method is called by the Trigger Base Class it gets the hash and checks the current hash or if its empty
     * @method _getTargets
     * @protected
     */
    p._getTargets = function() {

        this._hash = window.location.hash;

        var empty = this._hash === '' || this._hash === '#';

        if (empty && this._currentHash) {

            this._targets = this._getTargetElements(this._currentHash);
            this._hash = this._currentHash;
            this._currentHash = null;

        }
        else if (!empty && this._currentHash && this._checkCurrentTarget(this._currentHash)) {
            this._currentHash = this._hash;
        }
        else if (!empty) {

            this._targets = this._getTargetElements(this._hash);
            this._currentHash = this._hash;

        }

    };

    /**
     * This method is called by the Trigger Base Class and it listens to the window for hashchange
     * @method bindEvents
     * @public
     */
    p.bindEvents = function() {
        window.addEventListener('hashchange', this._trigger.bind(this));
    };

    /**
     * Get the targets and if the hash matches the targets href then call _toggle on the Base Class.
     * @method _trigger
     * @private
     */
    p._trigger = function() {

        this._getTargets();

        if (this._hash === this._href) {
            this._toggle();
        }

    };

    /**
     * Returns the targets as array without the hash sign.
     * @method _getTargetElements
     * @private
     * @param {String} targets
     */
    p._getTargetElements = function(targets) {
        return targets.substring(1).split('&');
    };

    /**
     * Gets the toggle through its ID with the Manager Class and checks if its not part of a group. Usefull if the hash gets resets or if the new target is a member of a (different) group.
     * @method _checkCurrentTarget
     * @private
     * @param {String} hash
     */
    p._checkCurrentTarget = function(hash) {

        var toggle = this._manager.getById(hash.substring(1), 'toggle');
        return toggle && !toggle.getGroup();

    };

    return HashTrigger;

}());