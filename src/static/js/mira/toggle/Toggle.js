/**
 * Toggle
 * Registers itself with the Toggle Manager, checks if its part of a group and sets the active and deactive state.
 *
 * @module mira/toggle/toggle
 */

// Register namespace
Namespace.register('mira.toggle').Toggle = (function() {

    'use strict';

    /**
     * @class Toggle
     * @constructor
     * @param {object} element. The HTML node wich should behave like a Toggle.
     */
    var Toggle = function(element) {

        this._element = element;
        this._attribute = 'data-toggle';
        this._id = this._element.getAttribute('id');

        // Register this Toggle with the Manager
        new mira.toggle.Manager.getInstance().register(this, 'toggle');

    };

    var p = Toggle.prototype;

    /**
     * Checks if its a group member otherwise it checks its current state and calls the activate or deactivate method
     * @method toggle
     * @public
     */
    p.toggle = function() {

        // if part of group let the group handle it
        if (this.getGroup()) {
            Observer.publish(this, 'toggle', this);
        }
        else {
            this.getState() === 'inactive' ? this.activate() : this.deactivate();
        }

    };

    /**
     * Sets the data-toggle attribute value for the HTML element to active
     * @method activate
     * @public
     */
    p.activate = function() {
        this._element.setAttribute(this._attribute, 'active');
        this._reflow(this._element);
        Observer.publish(this, 'active', this);

    };

    /**
     * Sets the data-toggle attribute value for the HTML element to inactive
     * @method deactivate
     * @public
     */
    p.deactivate = function() {

        this._element.setAttribute(this._attribute, 'inactive');
        this._reflow(this._element);

        Observer.publish(this, 'inactive', this);

    };

    /**
     * Returns the Toggles ID
     * @method getId
     * @public
     */
    p.getId = function() {
        return this._id;
    };

    /**
     * Returns the HTML element
     * @method getElement
     * @public
     */
    p.getElement = function() {
        return this._element;
    };

    /**
     * Returns the current attribute value of the HTML element that represents the current toggle state
     * @method getState
     * @public
     */
    p.getState = function() {
        return this._element.getAttribute(this._attribute);
    };

    /**
     * Returns the current attribute value of the HTML element that represents the group name for this toggle if its a member.
     * @method getGroup
     * @public
     */
    p.getGroup = function() {
        return this._element.getAttribute('data-group');
    };

    p._reflow = function(element) {
        element.className = element.className;
    };

    return Toggle;

}());