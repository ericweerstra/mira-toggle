/**
 * Trigger
 * This is the trigger base that the different trigger Classes use as a Subclass
 *
 * @module mira/toggle/trigger
 */

// Register namespace
Namespace.register('mira.toggle').Trigger = (function() {

    'use strict';

    /**
     * @class Trigger
     * @constructor
     * @param {object} element. The HTML node wich sometimes should behave like a trigger.
     */
    var Trigger = function(element) {

        this._element = element;
        this._id = element.getAttribute('id');
        this._targets = [];
        this._manager = new mira.toggle.Manager.getInstance();

        this._initialize();

    };

    var p = Trigger.prototype;

    /**
     * Initialization method that calls the methods that are used within the different Subclasses.
     * @method _initialize
     * @private
     */
    p._initialize = function() {

        // Register this Trigger with the Manager
        this._manager.register(this, 'trigger');

        this._setup();
        this._getTargets();
        this.bindEvents();

    };

    /**
     * This checks with the Manager if the target is a toggle and then calls the Toggle method on it.
     * @method _toggle
     * @protected
     */
    p._toggle = function() {

        var self = this,
            target;

        this._targets.forEach(function(element) {

            target = self._manager.getById(element, 'toggle');

            if (target) {
                target.toggle();
            }

        });

    };

    /**
     * Returns the Trigger ID
     * @method getId
     * @public
     */
    p.getId = function() {
        return this._id;
    };

    return Trigger;

}());