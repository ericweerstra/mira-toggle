/**
 * Main
 * Demo Startingpoint. Checks for DOM ready and then collects the nodes with the toggle or toggle-trigger attributes and creates Class instances with them.
 *
 * @module mira/core/main
 */

// Register namespace
Namespace.register('mira.core').Main = (function() {

    'use strict';

    /**
     * @class Main
     * @constructor
     */
    var Main = function() {

        // Create a Trigger Controler factory instance
        this._triggerController = new mira.toggle.TriggerController.getInstance();

        // Listen for the DOM to be ready and then initialize.
        document.addEventListener('DOMContentLoaded', this._initialize.bind(this));

    };

    // store the
    var p = Main.prototype;

    /**
     * Initialization method
     * @method initialize
     * @private
     */
    p._initialize = function() {

        this._createToggles();
        this._createTriggers();

    };

    /**
     * Gets the toggle nodes and creates instances of the Toggle Class.
     * @method _createToggles
     * @private
     */
    p._createToggles = function() {

        var toggleElements = document.querySelectorAll('[data-toggle]'),
            i, length;

        for (i = 0, length = toggleElements.length; i < length; i++) {
            new mira.toggle.Toggle(toggleElements[i]);
        }

    };

    /**
     * Gets the trigger nodes and creates instances of the Toggle Trigger Class.
     * @method _createTriggers
     * @private
     */
    p._createTriggers = function() {

        var triggerElements = document.querySelectorAll('[data-toggle-trigger]'),
            i, length;

        for (i = 0, length = triggerElements.length; i < length; i++) {
            this._triggerController.createTrigger(triggerElements[i]);
        }

    };


    /**
   	 * Stores the instance of this class if there is one.
   	 * @property _instance
   	 * @type {Object}
   	 * @default null
   	 **/
    var _instance;

    return {

        /**
       	 * Returns the instance object if there is one otherwise one will be created so there can only be one. This turns this Class into a Singleton.
         * @method getInstance
       	 * @private
       	 **/

        getInstance: function() {

            if (!_instance) {
                _instance = new Main();
            }

            return _instance;
        }
    };

})();